---
title: About me
comments: false
---

My name is Jad Ghalayini: I'm a systems developer and aspiring mathematician interested in the intersection between abstract mathematics and practical computation. I have a particular obsession with the study, design, and development of compilers. I graduated from the University of Toronto with a Mathematics Specialist and Computer Science Specialist, and am currently pursuing an MSc. in Computer Science at Oxford University, where I am a student at Green Templeton College.

I also:
- Write rather over-complicated science-fiction.
- Study languages as a hobby: I'm fluent in English and French and am studying Arabic and Mandarin.
- Really like food. Especially sushi.